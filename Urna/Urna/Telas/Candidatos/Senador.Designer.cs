﻿namespace Urna
{
    partial class Senador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPartido = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txt3 = new System.Windows.Forms.TextBox();
            this.txt2 = new System.Windows.Forms.TextBox();
            this.txt1 = new System.Windows.Forms.TextBox();
            this.lblNome = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pbxUm = new System.Windows.Forms.Button();
            this.pbxTres = new System.Windows.Forms.Button();
            this.pbxQuatro = new System.Windows.Forms.Button();
            this.pbxCinco = new System.Windows.Forms.Button();
            this.pbxSeis = new System.Windows.Forms.Button();
            this.pbxSete = new System.Windows.Forms.Button();
            this.pbxOito = new System.Windows.Forms.Button();
            this.pbxNove = new System.Windows.Forms.Button();
            this.pbxDez = new System.Windows.Forms.Button();
            this.pbxDois = new System.Windows.Forms.Button();
            this.btnConfirma = new System.Windows.Forms.Button();
            this.btnCorrige = new System.Windows.Forms.Button();
            this.btnBranco = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPartido
            // 
            this.lblPartido.AutoSize = true;
            this.lblPartido.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblPartido.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPartido.ForeColor = System.Drawing.Color.Black;
            this.lblPartido.Location = new System.Drawing.Point(71, 207);
            this.lblPartido.Name = "lblPartido";
            this.lblPartido.Size = new System.Drawing.Size(95, 18);
            this.lblPartido.TabIndex = 152;
            this.lblPartido.Text = "Republicano";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(1, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 18);
            this.label6.TabIndex = 150;
            this.label6.Text = "Número:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(5, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 18);
            this.label5.TabIndex = 149;
            this.label5.Text = "Partido:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(12, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 18);
            this.label3.TabIndex = 148;
            this.label3.Text = "Nome:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label8.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(46, 355);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(257, 22);
            this.label8.TabIndex = 147;
            this.label8.Text = "LARANJA para CORRIGIR ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label7.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(70, 333);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(252, 22);
            this.label7.TabIndex = 146;
            this.label7.Text = "VERDE para CONFIRMAR ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(4, 309);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 22);
            this.label1.TabIndex = 145;
            this.label1.Text = "Aperte a tecla:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.ForeColor = System.Drawing.Color.Black;
            this.panel1.Location = new System.Drawing.Point(-5, 303);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(517, 3);
            this.panel1.TabIndex = 144;
            // 
            // txt3
            // 
            this.txt3.Location = new System.Drawing.Point(140, 131);
            this.txt3.Multiline = true;
            this.txt3.Name = "txt3";
            this.txt3.Size = new System.Drawing.Size(27, 32);
            this.txt3.TabIndex = 142;
            // 
            // txt2
            // 
            this.txt2.Location = new System.Drawing.Point(107, 131);
            this.txt2.Multiline = true;
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(27, 32);
            this.txt2.TabIndex = 141;
            // 
            // txt1
            // 
            this.txt1.Location = new System.Drawing.Point(74, 131);
            this.txt1.Multiline = true;
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(27, 32);
            this.txt1.TabIndex = 140;
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblNome.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.ForeColor = System.Drawing.Color.Black;
            this.lblNome.Location = new System.Drawing.Point(71, 174);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(55, 18);
            this.lblNome.TabIndex = 139;
            this.lblNome.Text = "Fulano";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(158, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 24);
            this.label2.TabIndex = 138;
            this.label2.Text = "Senador 1";
            // 
            // pbxUm
            // 
            this.pbxUm.BackColor = System.Drawing.Color.DimGray;
            this.pbxUm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbxUm.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxUm.FlatAppearance.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.pbxUm.FlatAppearance.BorderSize = 2;
            this.pbxUm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pbxUm.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pbxUm.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pbxUm.Location = new System.Drawing.Point(535, 37);
            this.pbxUm.Name = "pbxUm";
            this.pbxUm.Size = new System.Drawing.Size(104, 63);
            this.pbxUm.TabIndex = 257;
            this.pbxUm.Text = "1";
            this.pbxUm.UseVisualStyleBackColor = false;
            this.pbxUm.Click += new System.EventHandler(this.pbxUm_Click);
            // 
            // pbxTres
            // 
            this.pbxTres.BackColor = System.Drawing.Color.DimGray;
            this.pbxTres.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbxTres.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxTres.FlatAppearance.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.pbxTres.FlatAppearance.BorderSize = 2;
            this.pbxTres.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pbxTres.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pbxTres.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pbxTres.Location = new System.Drawing.Point(755, 36);
            this.pbxTres.Name = "pbxTres";
            this.pbxTres.Size = new System.Drawing.Size(104, 63);
            this.pbxTres.TabIndex = 256;
            this.pbxTres.Text = "3";
            this.pbxTres.UseVisualStyleBackColor = false;
            this.pbxTres.Click += new System.EventHandler(this.pbxTres_Click);
            // 
            // pbxQuatro
            // 
            this.pbxQuatro.BackColor = System.Drawing.Color.DimGray;
            this.pbxQuatro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbxQuatro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxQuatro.FlatAppearance.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.pbxQuatro.FlatAppearance.BorderSize = 2;
            this.pbxQuatro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pbxQuatro.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pbxQuatro.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pbxQuatro.Location = new System.Drawing.Point(535, 106);
            this.pbxQuatro.Name = "pbxQuatro";
            this.pbxQuatro.Size = new System.Drawing.Size(104, 63);
            this.pbxQuatro.TabIndex = 255;
            this.pbxQuatro.Text = "4";
            this.pbxQuatro.UseVisualStyleBackColor = false;
            this.pbxQuatro.Click += new System.EventHandler(this.pbxQuatro_Click);
            // 
            // pbxCinco
            // 
            this.pbxCinco.BackColor = System.Drawing.Color.DimGray;
            this.pbxCinco.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbxCinco.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxCinco.FlatAppearance.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.pbxCinco.FlatAppearance.BorderSize = 2;
            this.pbxCinco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pbxCinco.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pbxCinco.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pbxCinco.Location = new System.Drawing.Point(645, 106);
            this.pbxCinco.Name = "pbxCinco";
            this.pbxCinco.Size = new System.Drawing.Size(104, 63);
            this.pbxCinco.TabIndex = 254;
            this.pbxCinco.Text = "5";
            this.pbxCinco.UseVisualStyleBackColor = false;
            this.pbxCinco.Click += new System.EventHandler(this.pbxCinco_Click);
            // 
            // pbxSeis
            // 
            this.pbxSeis.BackColor = System.Drawing.Color.DimGray;
            this.pbxSeis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbxSeis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxSeis.FlatAppearance.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.pbxSeis.FlatAppearance.BorderSize = 2;
            this.pbxSeis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pbxSeis.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pbxSeis.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pbxSeis.Location = new System.Drawing.Point(755, 106);
            this.pbxSeis.Name = "pbxSeis";
            this.pbxSeis.Size = new System.Drawing.Size(104, 63);
            this.pbxSeis.TabIndex = 253;
            this.pbxSeis.Text = "6";
            this.pbxSeis.UseVisualStyleBackColor = false;
            this.pbxSeis.Click += new System.EventHandler(this.pbxSeis_Click);
            // 
            // pbxSete
            // 
            this.pbxSete.BackColor = System.Drawing.Color.DimGray;
            this.pbxSete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbxSete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxSete.FlatAppearance.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.pbxSete.FlatAppearance.BorderSize = 2;
            this.pbxSete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pbxSete.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pbxSete.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pbxSete.Location = new System.Drawing.Point(535, 175);
            this.pbxSete.Name = "pbxSete";
            this.pbxSete.Size = new System.Drawing.Size(104, 63);
            this.pbxSete.TabIndex = 252;
            this.pbxSete.Text = "7";
            this.pbxSete.UseVisualStyleBackColor = false;
            this.pbxSete.Click += new System.EventHandler(this.pbxSete_Click);
            // 
            // pbxOito
            // 
            this.pbxOito.BackColor = System.Drawing.Color.DimGray;
            this.pbxOito.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbxOito.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxOito.FlatAppearance.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.pbxOito.FlatAppearance.BorderSize = 2;
            this.pbxOito.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pbxOito.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pbxOito.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pbxOito.Location = new System.Drawing.Point(645, 175);
            this.pbxOito.Name = "pbxOito";
            this.pbxOito.Size = new System.Drawing.Size(104, 63);
            this.pbxOito.TabIndex = 251;
            this.pbxOito.Text = "8";
            this.pbxOito.UseVisualStyleBackColor = false;
            this.pbxOito.Click += new System.EventHandler(this.pbxOito_Click);
            // 
            // pbxNove
            // 
            this.pbxNove.BackColor = System.Drawing.Color.DimGray;
            this.pbxNove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbxNove.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxNove.FlatAppearance.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.pbxNove.FlatAppearance.BorderSize = 2;
            this.pbxNove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pbxNove.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pbxNove.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pbxNove.Location = new System.Drawing.Point(755, 175);
            this.pbxNove.Name = "pbxNove";
            this.pbxNove.Size = new System.Drawing.Size(104, 63);
            this.pbxNove.TabIndex = 250;
            this.pbxNove.Text = "9";
            this.pbxNove.UseVisualStyleBackColor = false;
            this.pbxNove.Click += new System.EventHandler(this.pbxNove_Click);
            // 
            // pbxDez
            // 
            this.pbxDez.BackColor = System.Drawing.Color.DimGray;
            this.pbxDez.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbxDez.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxDez.FlatAppearance.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.pbxDez.FlatAppearance.BorderSize = 2;
            this.pbxDez.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pbxDez.Font = new System.Drawing.Font("Arial Narrow", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pbxDez.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pbxDez.Location = new System.Drawing.Point(645, 244);
            this.pbxDez.Name = "pbxDez";
            this.pbxDez.Size = new System.Drawing.Size(104, 63);
            this.pbxDez.TabIndex = 249;
            this.pbxDez.Text = "0";
            this.pbxDez.UseVisualStyleBackColor = false;
            this.pbxDez.Click += new System.EventHandler(this.pbxDez_Click);
            // 
            // pbxDois
            // 
            this.pbxDois.BackColor = System.Drawing.Color.DimGray;
            this.pbxDois.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbxDois.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbxDois.FlatAppearance.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.pbxDois.FlatAppearance.BorderSize = 2;
            this.pbxDois.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.pbxDois.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pbxDois.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pbxDois.Location = new System.Drawing.Point(645, 36);
            this.pbxDois.Name = "pbxDois";
            this.pbxDois.Size = new System.Drawing.Size(104, 63);
            this.pbxDois.TabIndex = 248;
            this.pbxDois.Text = "2";
            this.pbxDois.UseVisualStyleBackColor = false;
            this.pbxDois.Click += new System.EventHandler(this.pbxDois_Click);
            // 
            // btnConfirma
            // 
            this.btnConfirma.BackColor = System.Drawing.Color.ForestGreen;
            this.btnConfirma.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConfirma.FlatAppearance.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnConfirma.FlatAppearance.BorderSize = 2;
            this.btnConfirma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirma.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirma.ForeColor = System.Drawing.Color.Black;
            this.btnConfirma.Location = new System.Drawing.Point(755, 323);
            this.btnConfirma.Name = "btnConfirma";
            this.btnConfirma.Size = new System.Drawing.Size(104, 59);
            this.btnConfirma.TabIndex = 246;
            this.btnConfirma.Text = "CONFIRMA";
            this.btnConfirma.UseVisualStyleBackColor = false;
            this.btnConfirma.Click += new System.EventHandler(this.btnConfirma_Click);
            // 
            // btnCorrige
            // 
            this.btnCorrige.BackColor = System.Drawing.Color.Orange;
            this.btnCorrige.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCorrige.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCorrige.FlatAppearance.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnCorrige.FlatAppearance.BorderSize = 2;
            this.btnCorrige.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnCorrige.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCorrige.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCorrige.ForeColor = System.Drawing.Color.Black;
            this.btnCorrige.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnCorrige.Location = new System.Drawing.Point(645, 334);
            this.btnCorrige.Name = "btnCorrige";
            this.btnCorrige.Size = new System.Drawing.Size(104, 47);
            this.btnCorrige.TabIndex = 245;
            this.btnCorrige.Text = "CORRIGE";
            this.btnCorrige.UseVisualStyleBackColor = false;
            this.btnCorrige.Click += new System.EventHandler(this.btnCorrige_Click);
            // 
            // btnBranco
            // 
            this.btnBranco.BackColor = System.Drawing.Color.Ivory;
            this.btnBranco.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnBranco.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBranco.FlatAppearance.BorderColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnBranco.FlatAppearance.BorderSize = 2;
            this.btnBranco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBranco.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBranco.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnBranco.Location = new System.Drawing.Point(535, 334);
            this.btnBranco.Name = "btnBranco";
            this.btnBranco.Size = new System.Drawing.Size(104, 48);
            this.btnBranco.TabIndex = 244;
            this.btnBranco.Text = "BRANCO";
            this.btnBranco.UseVisualStyleBackColor = false;
            this.btnBranco.Click += new System.EventHandler(this.btnBranco_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Black;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Location = new System.Drawing.Point(489, -1);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(387, 405);
            this.pictureBox3.TabIndex = 247;
            this.pictureBox3.TabStop = false;
            // 
            // Senador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(888, 400);
            this.Controls.Add(this.pbxUm);
            this.Controls.Add(this.pbxTres);
            this.Controls.Add(this.pbxQuatro);
            this.Controls.Add(this.pbxCinco);
            this.Controls.Add(this.pbxSeis);
            this.Controls.Add(this.pbxSete);
            this.Controls.Add(this.pbxOito);
            this.Controls.Add(this.pbxNove);
            this.Controls.Add(this.pbxDez);
            this.Controls.Add(this.pbxDois);
            this.Controls.Add(this.btnConfirma);
            this.Controls.Add(this.btnCorrige);
            this.Controls.Add(this.btnBranco);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.lblPartido);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txt3);
            this.Controls.Add(this.txt2);
            this.Controls.Add(this.txt1);
            this.Controls.Add(this.lblNome);
            this.Controls.Add(this.label2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Senador";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Senador";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPartido;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txt3;
        private System.Windows.Forms.TextBox txt2;
        private System.Windows.Forms.TextBox txt1;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button pbxUm;
        private System.Windows.Forms.Button pbxTres;
        private System.Windows.Forms.Button pbxQuatro;
        private System.Windows.Forms.Button pbxCinco;
        private System.Windows.Forms.Button pbxSeis;
        private System.Windows.Forms.Button pbxSete;
        private System.Windows.Forms.Button pbxOito;
        private System.Windows.Forms.Button pbxNove;
        private System.Windows.Forms.Button pbxDez;
        private System.Windows.Forms.Button pbxDois;
        private System.Windows.Forms.Button btnConfirma;
        private System.Windows.Forms.Button btnCorrige;
        private System.Windows.Forms.Button btnBranco;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}