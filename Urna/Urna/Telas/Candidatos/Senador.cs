﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.DB.Candidatos.Senador;
using Urna.DB.Eleitor;
using Urna.Telas;

namespace Urna
{
    public partial class Senador : Form
    {
        public Senador()
        {
            InitializeComponent();

            while (txt1.Text != string.Empty || txt2.Text != string.Empty || txt3.Text != string.Empty)
            {
                VerificarDeputado();
            }

        }

        public void VerificarDeputado()
        {
            string voto = txt1.Text + txt2.Text + txt3.Text;

            SenadorBusiness buss = new SenadorBusiness();
            SenadorDTO deputado = buss.Consultar(voto);

            if (deputado != null)
            {
                lblNome.Text = deputado.Nome;
                lblPartido.Text = Convert.ToString(deputado.IdPartido);
            }
            else
            {
                MessageBox.Show("Este número não está ligado a nenhum Senador.", "Urna", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
           
        }


        public void EnviarDptFederal()
        {
            string voto = txt1.Text + txt2.Text + txt3.Text;

            Votos.Senador = voto;
        }

        public int MandarVoto()
        {
            string voto = txt1.Text + txt2.Text + txt3.Text;

            int votado = int.Parse(voto);
            return votado;
        }

        private void btnConfirma_Click(object sender, EventArgs e)
        {
            string voto = txt1.Text + txt2.Text + txt3.Text;

            SenadorBusiness buss = new SenadorBusiness();
            SenadorDTO deputado = buss.Consultar(voto);

            if (deputado != null)
            {

                EnviarDptFederal();

                MessageBox.Show("Voto Concluido!", "Urna", MessageBoxButtons.OK);
                Senador2 tela = new Senador2();
                tela.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Este número não está ligado a nenhum Senador.", "Urna", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void pbxDois_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "2";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty)
            {
                txt2.Text = "2";
            }
            else
            {
                txt3.Text = "2";
            }
        }

        private void pbxTres_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "3";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty)
            {
                txt2.Text = "3";
            }
            else
            {
                txt3.Text = "3";
            }
        }

        private void pbxQuatro_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "4";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty)
            {
                txt2.Text = "4";
            }
            else
            {
                txt3.Text = "4";
            }
        }

        private void pbxCinco_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "5";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty)
            {
                txt2.Text = "5";
            }
            else
            {
                txt3.Text = "5";
            }
        }

        private void pbxSeis_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "6";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty)
            {
                txt2.Text = "6";
            }
            else
            {
                txt3.Text = "6";
            }
        }

        private void pbxSete_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "7";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty)
            {
                txt2.Text = "7";
            }
            else
            {
                txt3.Text = "7";
            }
        }

        private void pbxOito_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "8";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty)
            {
                txt2.Text = "8";
            }
            else
            {
                txt3.Text = "8";
            }
        }

        private void pbxNove_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "9";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty)
            {
                txt2.Text = "9";
            }
            else
            {
                txt3.Text = "9";
            }
        }

        private void pbxDez_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "0";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty)
            {
                txt2.Text = "0";
            }
            else
            {
                txt3.Text = "0";
            }
        }

       
        private void pbxUm_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty)
            {
                txt1.Text = "1";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty)
            {
                txt2.Text = "1";
            }
            else
            {
                txt3.Text = "1";
            }
        }

        private void btnCorrige_Click(object sender, EventArgs e)
        {
            txt1.Text = null;
            txt2.Text = null;
            txt3.Text = null;
        }

        private void btnBranco_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Voto Concluido!", "Urna", MessageBoxButtons.OK);
            Senador2 tela = new Senador2();
            tela.Show();
            this.Hide();
        }
    }
}
