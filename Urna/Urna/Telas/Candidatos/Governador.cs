﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.DB.Candidatos.Governador;
using Urna.DB.Eleitor;

namespace Urna.Telas
{
    public partial class Governador : Form
    {
        public Governador()
        {
            InitializeComponent();

            while (txt1.Text != string.Empty || txt2.Text != string.Empty)
            {
                VerificarDeputado();
            }
        }

        private void pbxUm_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "1";
            }
            else
            {
                txt2.Text = "1";
            }
           
        }

        private void pbxDois_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "2";
            }
            else
            {
                txt2.Text = "2";
            }
        }

        private void pbxTres_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "3";
            }
            else
            {
                txt2.Text = "3";
            }
        }

        private void pbxQuatro_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "4";
            }
            else
            {
                txt2.Text = "4";
            }
        }

        private void pbxCinco_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "5";
            }
            else
            { 
                txt2.Text = "5";
            }
        }

        private void pbxSeis_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "6";
            }
            else
            {
                txt2.Text = "6";
            }
        }

        private void pbxSete_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "7";
            }
            else
            {
                txt2.Text = "7";
            }
        }

        private void pbxOito_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "8";
            }
            else
            {
                txt2.Text = "8";
            }
        }

        private void pbxNove_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "9";
            }
            else
            {
                txt2.Text = "9";
            }
        }


        private void pbxDez_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty)
            {
                txt1.Text = "0";
            }
            else
            {
                txt2.Text = "0";
            }
        }


        public void EnviarDptFederal()
        {
            string voto = txt1.Text + txt2.Text;

            Votos.Governador = voto;
        }

        public void VerificarDeputado()
        {
            string voto = txt1.Text + txt2.Text;

            GovernadorBusiness buss = new GovernadorBusiness();
            GovernadorDTO deputado = buss.Consultar(voto);

            if (deputado != null)
            {
                lblNome.Text = deputado.Nome;
                lblPartido.Text = Convert.ToString(deputado.IdPartido);
                lblVice.Text = deputado.Vice;
            }
            else
            {
                MessageBox.Show("Este número não está ligado a nenhum Governador.", "Urna", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        public int MandarVoto()
        {
            string voto = txt1.Text + txt2.Text;

            int votado = int.Parse(voto);
            return votado;
        }
        private void btnConfirma_Click(object sender, EventArgs e)
        {
            string voto = txt1.Text + txt2.Text;

            GovernadorBusiness buss = new GovernadorBusiness();
            GovernadorDTO deputado = buss.Consultar(voto);

            if (deputado != null)
            {

                EnviarDptFederal();

                MessageBox.Show("Voto Concluido!", "Urna", MessageBoxButtons.OK);
                Presidente tela = new Presidente();
                tela.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Este número não está ligado a nenhum Governador.", "Urna", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnCorrige_Click(object sender, EventArgs e)
        {
            txt1.Text = null;
            txt2.Text = null;
        }

        private void btnBranco_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Voto Concluido!", "Urna", MessageBoxButtons.OK);
            Presidente tela = new Presidente();
            tela.Show();
            this.Hide();
        }
    }
}
