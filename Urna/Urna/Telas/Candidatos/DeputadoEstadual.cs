﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.DB.Candidatos.Deputado_Estadual;
using Urna.DB.Eleitor;
using Urna.Telas;

namespace Urna
{
    public partial class DeputadoEstadual : Form
    {
        public DeputadoEstadual()
        {
            InitializeComponent();

            while (txt1.Text != string.Empty || txt2.Text != string.Empty || txt2.Text != string.Empty || txt4.Text != string.Empty)
            {
                VerificarDeputado();
            }
        }

        private void DeputadoEstadual_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {

        }

        private void button11_Click(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {

        }

        private void button13_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {

        }

        private void button10_Click(object sender, EventArgs e)
        {

        }

        private void pbxUm_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt1.Text = "1";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt2.Text = "1";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt3.Text = "1";
            }
            else if (txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt4.Text = "1";
            }
            else
            {
                txt5.Text = "1";
            }
        }

        private void pbxDois_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt1.Text = "2";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt2.Text = "2";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt3.Text = "2";
            }
            else if (txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt4.Text = "2";
            }
            else
            {
                txt5.Text = "2";
            }
        }

        private void pbxTres_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt1.Text = "3";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt2.Text = "3";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt3.Text = "3";
            }
            else if (txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt4.Text = "3";
            }
            else
            {
                txt5.Text = "3";
            }
        }

        private void pbxQuatro_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt1.Text = "4";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt2.Text = "4";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt3.Text = "4";
            }
            else if (txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt4.Text = "4";
            }
            else
            {
                txt5.Text = "4";
            }
        }

        private void pbxCinco_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt1.Text = "5";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt2.Text = "5";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt3.Text = "5";
            }
            else if (txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt4.Text = "5";
            }
            else
            {
                txt5.Text = "5";
            }
        }

        private void pbxSeis_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt1.Text = "6";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt2.Text = "6";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt3.Text = "6";
            }
            else if (txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt4.Text = "6";
            }
            else
            {
                txt5.Text = "6";
            }
        }

        private void pbxSete_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt1.Text = "7";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt2.Text = "7";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt3.Text = "7";
            }
            else if (txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt4.Text = "7";
            }
            else
            {
                txt5.Text = "7";
            }
        }

        private void pbxOito_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt1.Text = "8";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt2.Text = "8";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt3.Text = "8";
            }
            else if (txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt4.Text = "8";
            }
            else
            {
                txt5.Text = "8";
            }
        }

        private void pbxNove_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt1.Text = "9";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt2.Text = "9";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt3.Text = "9";
            }
            else if (txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt4.Text = "9";
            }
            else
            {
                txt5.Text = "9";
            }
        }

        private void pbxDez_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt1.Text = "0";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt2.Text = "0";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt3.Text = "0";
            }
            else if (txt4.Text == string.Empty && txt5.Text == string.Empty)
            {
                txt4.Text = "0";
            }
            else
            {
                txt5.Text = "0";
            }
        }

        private void btnCorrige_Click(object sender, EventArgs e)
        {
            txt1.Text = null;
            txt2.Text = null;
            txt3.Text = null;
            txt4.Text = null;
            txt5.Text = null;
        }

        public void EnviarDptFederal()
        {
            string voto = txt1.Text + txt2.Text + txt3.Text + txt4.Text + txt5.Text;
;
            Votos.DptEstadual = voto;

        }

        public int MandarVoto()
        {
            string voto = txt1.Text + txt2.Text + txt3.Text + txt4.Text + txt5.Text;

            int votado = int.Parse(voto);
            return votado;
        }

        public void VerificarDeputado()
        {
            string voto = txt1.Text + txt2.Text + txt3.Text + txt4.Text + txt5.Text;

            DeputadoEstadualBusiness buss = new DeputadoEstadualBusiness();
            DeputadoEstadualDTO deputado = buss.Consultar(voto);

            if (deputado != null)
            {
                lblNome.Text = deputado.Nome;
                lblPartido.Text = Convert.ToString(deputado.IdPartido);
            }
            else
            {
                MessageBox.Show("Este número não está ligado a nenhum Deputado Estadual.", "Urna", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void btnConfirma_Click(object sender, EventArgs e)
        {
            string voto = txt1.Text + txt2.Text + txt3.Text + txt4.Text + txt5.Text;

            DeputadoEstadualBusiness buss = new DeputadoEstadualBusiness();
            DeputadoEstadualDTO deputado = buss.Consultar(voto);

            if (deputado != null)
            {

                EnviarDptFederal();

                MessageBox.Show("Voto Concluido!", "Urna", MessageBoxButtons.OK);
                Governador tela = new Governador();
                tela.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Este número não está ligado a nenhum Deputado Estadual.", "Urna", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnBranco_Click(object sender, EventArgs e)
        {

            MessageBox.Show("Voto Concluido!", "Urna", MessageBoxButtons.OK);
            Governador tela = new Governador();
            tela.Show();
            this.Hide();
        }
    }
}
