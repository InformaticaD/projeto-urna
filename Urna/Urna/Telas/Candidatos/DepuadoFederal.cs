﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.DB.Base;
using Urna.DB.Candidatos.Deputado_Federal;
using Urna.DB.Eleitor;

namespace Urna
{
    public partial class DepuadoFederal : Form
    {
        public DepuadoFederal()
        {
            InitializeComponent();

            while (txt1.Text != string.Empty || txt2.Text != string.Empty || txt2.Text != string.Empty || txt4.Text != string.Empty)
            {
                VerificarDeputado();
            }

        }


        private void DepuadoFederal_Load(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        public void VerificarDeputado()
        {
            string voto = txt1.Text + txt2.Text + txt3.Text + txt4.Text;

            DeputadoFederalBusiness buss = new DeputadoFederalBusiness();
            DeputadoFederalDTO deputado = buss.Consultar(voto);

            if (deputado != null)
            {
                lblNome.Text = deputado.Nome;
                lblPartido.Text = deputado.IdPartido.ToString();
            }
            else
            {
                MessageBox.Show("Este número não está ligado a nenhum Deputado Federal.", "Urna", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }



            
        }

        public int MandarVoto()
        {
            string voto = txt1.Text + txt2.Text + txt3.Text + txt4.Text;
            int votado = votado = int.Parse(voto);

            return votado;
                     
        }

        public void EnviarDptFederal()
        {
            MandarVoto();

            
            Votos.DptFederal = Convert.ToString(MandarVoto());

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string voto = txt1.Text + txt2.Text + txt3.Text + txt4.Text;

            DeputadoFederalBusiness buss = new DeputadoFederalBusiness();
            DeputadoFederalDTO deputado = buss.Consultar(voto);

            if (deputado != null)
            {

                EnviarDptFederal();

                MessageBox.Show("Voto Concluido!", "Urna", MessageBoxButtons.OK);
                DeputadoEstadual tela = new DeputadoEstadual();
                tela.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Este número não está ligado a nenhum Deputado Federal.", "Urna", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt1.Text = "1";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt2.Text = "1";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt3.Text = "1";
            }
            else
            {
                txt4.Text = "1";
            }

        }

        private void txt1_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void pbxDois_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt1.Text = "2";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt2.Text = "2";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt3.Text = "2";
            }
            else
            {
                txt4.Text = "2";
            }
        }

        private void pbxTres_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt1.Text = "3";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt2.Text = "3";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt3.Text = "3";
            }
            else
            {
                txt4.Text = "3";
            }
        }

        private void pbxQuatro_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt1.Text = "4";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt2.Text = "4";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt3.Text = "4";
            }
            else
            {
                txt4.Text = "4";
            }
        }

        private void pbxCinco_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt1.Text = "5";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt2.Text = "5";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt3.Text = "5";
            }
            else
            {
                txt4.Text = "5";
            }
        }

        private void pbxSeis_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt1.Text = "6";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt2.Text = "6";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt3.Text = "6";
            }
            else
            {
                txt4.Text = "6";
            }
        }

        private void pbxSete_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt1.Text = "7";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt2.Text = "7";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt3.Text = "7";
            }
            else
            {
                txt4.Text = "7";
            }
        }

        private void pbxOito_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt1.Text = "8";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt2.Text = "8";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt3.Text = "8";
            }
            else
            {
                txt4.Text = "8";
            }
        }

        private void pbxNove_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt1.Text = "9";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt2.Text = "9";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt3.Text = "9";
            }
            else
            {
                txt4.Text = "9";
            }

        }

        private void pbxDez_Click(object sender, EventArgs e)
        {
            if (txt1.Text == string.Empty && txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt1.Text = "0";
            }
            else if (txt2.Text == string.Empty && txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt2.Text = "0";
            }
            else if (txt3.Text == string.Empty && txt4.Text == string.Empty)
            {
                txt3.Text = "0";
            }
            else
            {
                txt4.Text = "0";
            }
        }

        private void btnCorrige_Click(object sender, EventArgs e)
        {
            txt1.Text = null;
            txt2.Text = null;
            txt3.Text = null;
            txt4.Text = null;
        }

        private void btnBranco_Click(object sender, EventArgs e)
        {

            MessageBox.Show("Voto Concluido!", "Urna", MessageBoxButtons.OK);
            DeputadoEstadual tela = new DeputadoEstadual();
            tela.Show();
            this.Hide();
        }

        private void txt4_TextChanged(object sender, EventArgs e)
        {
            if (txt4.Text != string.Empty)
            {
                VerificarDeputado();
            }
        }

       
    }
}
