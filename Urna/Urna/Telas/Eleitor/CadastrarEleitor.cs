﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.DB.Eleitor;

namespace Urna.Telas.Eleitor
{
    public partial class CadastrarEleitor : Form
    {
        public CadastrarEleitor()
        {
            InitializeComponent();
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            EleitorDTO dto = new EleitorDTO();

            dto.Nome = txtNome.Text;
            dto.Numero = txtNumero.Text;

            EleitorBusiness buss = new EleitorBusiness();
            buss.Salvar(dto);

            MessageBox.Show("Eleitor salvo com sucesso!", "Urna", MessageBoxButtons.OK);

            frmEleitor tela = new frmEleitor();
            tela.Show();
            this.Close();
        }
    }
}
