﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Urna.Telas.Eleitor;

namespace Urna
{
    public partial class frmEleitor : Form
    {
        public frmEleitor()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CadastrarEleitor tela = new CadastrarEleitor();
            tela.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DB.Eleitor.EleitorDTO dto = new DB.Eleitor.EleitorDTO();
            string num = txtNum.Text;

            DB.Eleitor.EleitorBusiness buss = new DB.Eleitor.EleitorBusiness();
            buss.Consultar(num);

            DepuadoFederal tela = new DepuadoFederal();
            tela.Show();
            this.Hide();
        }
    }
}
