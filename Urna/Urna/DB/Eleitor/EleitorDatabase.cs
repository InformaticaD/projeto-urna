﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Base;

namespace Urna.DB.Eleitor
{
    class EleitorDatabase
    {

         public int Salvar(EleitorDTO dto)
        {
            string script = @"INSERT INTO Eleitor(nm_eleitor,nr_tituloDeEleitor) VALUES (@nm_eleitor,@nr_tituloDeEleitor)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_eleitor", dto.Nome));
            parms.Add(new MySqlParameter("nr_tituloDeEleitor", dto.Numero));
          

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<EleitorDTO> Consultar(string eleitor)
        {
            string script =
            @"SELECT * 
                FROM eleitor
               WHERE nr_tituloDeEleitor like @nr_tituloDeEleitor";


            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nr_tituloDeEleitor", "%" + eleitor + "%"));


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EleitorDTO> Eleitores = new List<EleitorDTO>();

            while (reader.Read())
            {
                EleitorDTO marcas = new EleitorDTO();
                marcas.Id = reader.GetInt32("id_eleitor");
                marcas.Nome = reader.GetString("nm_eleitor");
                marcas.Numero = reader.GetString("nr_tituloDeEleitor");




                Eleitores.Add(marcas);
            }
            reader.Close();

            return Eleitores;
        }
    }
}
