﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Eleitor
{
    class EleitorBusiness
    {
        EleitorDatabase db = new EleitorDatabase();

        public int Salvar(EleitorDTO Eleitor)
        {
            if (Eleitor.Numero == string.Empty)
            {
                throw new ArgumentException("Número é obrigatório.");
            }


            return db.Salvar(Eleitor);
        }

        public List<EleitorDTO> Consultar(string Eleitor)
        {

            return db.Consultar(Eleitor);
        }
    }
}
