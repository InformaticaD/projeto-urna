﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Eleitor
{
   public class EleitorDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Numero { get; set; }
    }
}
