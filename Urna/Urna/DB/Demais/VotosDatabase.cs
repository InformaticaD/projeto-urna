﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Base;

namespace Urna.DB.Demais
{
    class VotosDatabase
    {
        public int Salvar(VotosDTO dto)
        {
            string script = @"INSERT INTO votos (fk_votos_eleitor,fk_votos_deputadofederal,fk_votos_deputadoestadual,fk_votos_presidente,fk_votos_senador,fk_votos_senador2,fk_votos_governador) VALUES (@fk_votos_eleitor,@fk_votos_deputadofederal,@fk_votos_deputadoestadual,@fk_votos_presidente,@fk_votos_senador,@fk_votos_senador,@fk_votos_governador)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_votos_eleitor", dto.VtEleitor));
            parms.Add(new MySqlParameter("fk_votos_deputadofederal", dto.VtDptFederal));
            parms.Add(new MySqlParameter("fk_votos_deputadoestadual", dto.VtDptEstadual));
            parms.Add(new MySqlParameter("fk_votos_presidente", dto.VtPresidente));
            parms.Add(new MySqlParameter("fk_votos_senador", dto.VtSenador));
            parms.Add(new MySqlParameter("fk_votos_senador2", dto.VtSenador));
            parms.Add(new MySqlParameter("fk_votos_governador", dto.VtGovernador));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<VotosDTO> Consultar()
        {
            string script =
            @"SELECT * FROM votos";
              

        
          


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<VotosDTO> Partidos = new List<VotosDTO>();

            while (reader.Read())
            {
                VotosDTO partido = new VotosDTO();
                partido.Id = reader.GetInt32("id_votos");
                partido.VtDptEstadual = reader.GetInt32("fk_votos_deputadoestadual");
                partido.VtDptFederal = reader.GetInt32("fk_votos_deputadofederal");
                partido.VtSenador = reader.GetInt32("fk_votos_senador");
                partido.VtSenador2 = reader.GetInt32("fk_votos_senador2");
                partido.VtPresidente = reader.GetInt32("fk_votos_presidente");
                partido.VtGovernador = reader.GetInt32("fk_votos_governador");

                Partidos.Add(partido);
            }
            reader.Close();

            return Partidos;
        }
    }
}
