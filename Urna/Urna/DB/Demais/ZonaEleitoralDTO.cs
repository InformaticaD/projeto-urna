﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Demais
{
    class ZonaEleitoralDTO
    {
        public int Id { get; set; }
        public int IdEleitor { get; set; }
        public int IdZona { get; set; }
    }
}
