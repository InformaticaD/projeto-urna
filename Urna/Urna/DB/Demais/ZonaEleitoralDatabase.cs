﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Base;

namespace Urna.DB.Demais
{
    class ZonaEleitoralDatabase
    {
       public int Salvar(ZonaEleitoralDTO dto)
        {
            string script = @"INSERT INTO zona eleitoral (eleitor_id_eleitor, zona de partido_id_Zona-de-Partido) VALUES (@eleitor_id_eleitor,@zona de partido_id_Zona-de-Partido)";

          List<MySqlParameter> parms = new List<MySqlParameter>();
          parms.Add(new MySqlParameter("eleitor_id_eleitor", dto.IdEleitor));
          parms.Add(new MySqlParameter("zona de partido_id_Zona-de-Partido", dto.IdZona));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<ZonaEleitoralDTO> Consultar(string Eleitor)
        {
            string script =
            @"SELECT * 
                FROM zona eleitoral
               WHERE eleitor_id_eleitor like @eleitor_id_eleitor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_modelo", "%" + Eleitor + "%"));


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ZonaEleitoralDTO> Partidos = new List<ZonaEleitoralDTO>();

            while (reader.Read())
            {
                ZonaEleitoralDTO partido = new ZonaEleitoralDTO();
                partido.Id = reader.GetInt32("id_Zona_eleitoral");
                partido.IdEleitor = reader.GetInt32("eleitor_id_eleitor");
                partido.IdZona = reader.GetInt32("zona de partido_id_Zona-de-Partido");


                Partidos.Add(partido);
            }
            reader.Close();
           
            return Partidos;
        }
    }
}
