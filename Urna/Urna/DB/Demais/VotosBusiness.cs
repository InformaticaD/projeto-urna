﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Demais
{
    class VotosBusiness
    {
        VotosDatabase db = new VotosDatabase();

        public int Salvar(VotosDTO nome)
        {
            return db.Salvar(nome);
        }

        public List<VotosDTO> Consultar()
        {

            return db.Consultar();
        }
    }
}
