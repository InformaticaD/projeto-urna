﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Demais
{
    class ZonaPartidaDTO
    {
        public int Id { get; set; }
        public int Zona { get; set; }
        public int Secao { get; set; }
    }
}
