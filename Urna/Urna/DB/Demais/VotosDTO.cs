﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Demais
{
    class VotosDTO
    {
        public int Id { get; set; }
        public int VtDptFederal { get; set; }
        public int VtDptEstadual { get; set; }
        public int VtPresidente { get; set; }
        public int VtGovernador { get; set; }
        public int VtSenador { get; set; }
        public int VtSenador2 { get; set; }
        public int VtEleitor { get; set; }
    }
}
