﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Base;

namespace Urna.DB.Demais
{
    class PartidoDatabase
    {
        public int Salvar(PartidoDTO dto)
        {
            string script = @"INSERT INTO partido (nm_partido,ds_partido) VALUES (@nm_partido,@ds_partido)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_partido", dto.Nome));
            parms.Add(new MySqlParameter("ds_partido", dto.Descricao));
            

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<PartidoDTO> Consultar(string Partido)
        {
            string script =
            @"SELECT * 
                FROM partido
               WHERE nm_partido like @nm_partido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_modelo", "%" + Partido + "%"));
            

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PartidoDTO> Partidos = new List<PartidoDTO>();

            while (reader.Read())
            {
                PartidoDTO partido = new PartidoDTO();
                partido.Id = reader.GetInt32("id_partido");
                partido.Nome = reader.GetString("nm_partido");
                partido.Descricao = reader.GetString("ds_partido");
             

                Partidos.Add(partido);
            }
            reader.Close();

            return Partidos;
        }
    }
}
