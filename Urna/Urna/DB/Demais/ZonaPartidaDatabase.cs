﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Base;

namespace Urna.DB.Demais
{
    class ZonaPartidaDatabase
    {
        public int Salvar(ZonaPartidaDTO dto)
        {
            string script = @"INSERT INTO zona de partido (nr_zona-eleitoral,nr_secao) VALUES (@nr_zona-eleitoral,@nr_secao)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nr_zona-eleitoral", dto.Zona));
            parms.Add(new MySqlParameter("nr_secao", dto.Secao));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<ZonaPartidaDTO> Consultar(string Zona, string Secao)
        {
            string script =
            @"SELECT * 
                FROM partido
               WHERE nr_zona-eleitoral like @nr_zona-eleitoral
                 AND nr_secao like @nr_secao";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_modelo", "%" + Zona + "%"));
            parms.Add(new MySqlParameter("ds_modelo", "%" + Secao + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ZonaPartidaDTO> Eleitoral = new List<ZonaPartidaDTO>();

            while (reader.Read())
            {
                ZonaPartidaDTO eleitor = new ZonaPartidaDTO();
                eleitor.Id = reader.GetInt32("id_Zona-de-partido");
                eleitor.Zona = reader.GetInt32("nr_zona-eleitoral");
                eleitor.Secao = reader.GetInt32("nr_secao");


                Eleitoral.Add(eleitor);
            }
            reader.Close();

            return Eleitoral;
        }
    }
}
