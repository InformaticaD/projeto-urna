﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Demais;

namespace Urna.DB.Eleitor
{
    static class Votos
    {
        public static string DptFederal { get; set; }
        public static string DptEstadual { get; set; }
        public static string Governador { get; set; }
        public static string Presidente { get; set; }
        public static string Senador { get; set; }
        public static string Senador2 { get; set; }
        
        public static void MandarProDTO()
        {

            VotosDTO dto = new VotosDTO();

            dto.VtDptFederal = Convert.ToInt32(DptFederal);
            dto.VtDptEstadual = Convert.ToInt32(DptEstadual);
            dto.VtGovernador = Convert.ToInt32(Governador);
            dto.VtPresidente = Convert.ToInt32(Presidente);
            dto.VtSenador = Convert.ToInt32(Senador);
            dto.VtEleitor = Convert.ToInt32(Senador2);

            VotosBusiness buss = new VotosBusiness();
            buss.Salvar(dto);
        }
    }
}
