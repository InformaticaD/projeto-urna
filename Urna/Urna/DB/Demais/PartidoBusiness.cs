﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Demais
{
    class PartidoBusiness
    {
        PartidoDatabase db = new PartidoDatabase();

        public int Salvar(PartidoDTO nome)
        {
            if (nome.Nome == string.Empty)
            {
                throw new ArgumentException("Marca é obrigatória.");
            }
          

            return db.Salvar(nome);
        }

        public List<PartidoDTO> Consultar(string Partido)
        {

            return db.Consultar(Partido);
        }
    }
}
