﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Demais
{
    class ZonaPartidaBusiness
    {
        ZonaPartidaDatabase db = new ZonaPartidaDatabase();

        public int Salvar(ZonaPartidaDTO nome)
        {
            if (nome.Zona == 0)
            {
                throw new ArgumentException("Marca é obrigatória.");
            }
            if (nome.Secao == 0)
            {
                throw new ArgumentException("Marca é obrigatória.");
            }

            return db.Salvar(nome);
        }

        public List<ZonaPartidaDTO> Consultar(string Zona, string Secao)
        {

            return db.Consultar(Zona, Secao);
        }
    }
}
