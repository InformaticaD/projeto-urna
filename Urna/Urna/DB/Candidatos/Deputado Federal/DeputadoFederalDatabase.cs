﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Base;

namespace Urna.DB.Candidatos.Deputado_Federal
{
    public class DeputadoFederalDatabase
    {
        public int Salvar(DeputadoFederalDTO dto)
        {
            string script = @"INSERT INTO DeputadoFederal (partido_id_partido,nm_deputadofed,ft_deputadofed,nr_deputadofed) VALUES (@partido_id_partido,@nm_deputadofed,@ft_deputadofed,@nr_deputadofed)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("partido_id_partido", dto.IdPartido));
            parms.Add(new MySqlParameter("nm_deputadofed", dto.Nome));
            parms.Add(new MySqlParameter("ft_deputadofed", dto.Foto));
            parms.Add(new MySqlParameter("nr_deputadofed", dto.Numero));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public DeputadoFederalDTO Consultar(string numero)
        {
            string script =
            @"SELECT * 
                FROM DeputadoFederal
               WHERE nr_deputadofed like @nr_deputadofed";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nr_deputadofed", numero));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            DeputadoFederalDTO deputado = null;

            if (reader.Read())
            {
                deputado = new DeputadoFederalDTO();
                deputado.Id = reader.GetInt32("id_dptfederal");
                deputado.IdPartido = reader.GetInt32("Partido_id_Partido");
                deputado.Nome = reader.GetString("nm_deputadoFed");
                deputado.Numero = reader.GetInt32("nr_deputadoFed");
                deputado.Foto = reader.GetString("ft_deputadoFed");
              
            }
            reader.Close();

            return deputado;
        }

    }
}
