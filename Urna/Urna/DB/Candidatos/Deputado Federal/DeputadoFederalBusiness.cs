﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Candidatos.Deputado_Federal
{
    public class DeputadoFederalBusiness
    {
        

        public int Salvar(DeputadoFederalDTO nome)
        {
            DeputadoFederalDatabase db = new DeputadoFederalDatabase();
            if (nome.Numero == 0)
            {
                throw new ArgumentException("Número é obrigatório.");
            }

            return db.Salvar(nome);
        }

        public DeputadoFederalDTO Consultar(string numero)
        {
            DeputadoFederalDatabase db = new DeputadoFederalDatabase();
            return db.Consultar(numero);
        }
    }
}
