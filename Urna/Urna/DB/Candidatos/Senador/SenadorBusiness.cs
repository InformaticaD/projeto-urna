﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Candidatos.Senador
{
    public class SenadorBusiness
    {



        public int Salvar(SenadorDTO nome)
        {
            SenadorDatabase db = new SenadorDatabase();
            if (nome.Nome == string.Empty)
            {
                throw new ArgumentException("Marca é obrigatória.");
            }
            if (nome.Numero == 0)
            {
                throw new ArgumentException("Marca é obrigatória.");
            }

            return db.Salvar(nome);
        }

        public SenadorDTO Consultar(string numero)
        {
            SenadorDatabase db = new SenadorDatabase();
            return db.Consultar(numero);
        }

    }
}
