﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Candidatos.Senador
{
    public class SenadorDTO
    {

        public int Id { get; set; }
        public int IdPartido { get; set; }
        public string Nome { get; set; }
        public string Foto { get; set; }

        public int Numero { get; set; }
    }
}
