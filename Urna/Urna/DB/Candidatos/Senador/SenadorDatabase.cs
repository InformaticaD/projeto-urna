﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Base;

namespace Urna.DB.Candidatos.Senador
{
    public class SenadorDatabase
    {
        public int Salvar(SenadorDTO dto)
        {
            string script = @"INSERT INTO Senador (partido_id_partido,nm_senador,ft_senador,nr_senador) VALUES (@partido_id_partido,@nm_senador,@ft_senador,@nr_senador)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("partido_id_partido", dto.IdPartido));
            parms.Add(new MySqlParameter("nm_senador", dto.Nome));
            parms.Add(new MySqlParameter("ft_senador", dto.Foto));
            parms.Add(new MySqlParameter("nr_senador", dto.Numero));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public SenadorDTO Consultar(string numero)
        {
            string script =
            @"SELECT * 
                FROM Senador WHERE
                 nr_senador like @nr_senador";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nr_senador",numero));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            SenadorDTO Senadores = null;

            if (reader.Read())
            {
                Senadores = new SenadorDTO();
                Senadores.Id = reader.GetInt32("id_senador");
                Senadores.IdPartido = reader.GetInt32("partido_id_partido");
                Senadores.Nome = reader.GetString("nm_Senador");
                Senadores.Numero = reader.GetInt32("nr_Senador");
                Senadores.Foto = reader.GetString("ft_Senador");
            }
            reader.Close();

            return Senadores;
        }
    }
}
