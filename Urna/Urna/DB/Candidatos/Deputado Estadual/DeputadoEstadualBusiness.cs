﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Candidatos.Deputado_Estadual
{
    public class DeputadoEstadualBusiness
    {
        

        public int Salvar(DeputadoEstadualDTO nome)
        {
            DeputadoEstadualDatabase db = new DeputadoEstadualDatabase();
            if (nome.Numero == 0)
            {
                throw new ArgumentException("Número é obrigatório.");
            }

            return db.Salvar(nome);
        }

        public DeputadoEstadualDTO Consultar(string numero)
        {
            DeputadoEstadualDatabase db = new DeputadoEstadualDatabase();
            return db.Consultar(numero);
        }
    }
}
