﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Base;

namespace Urna.DB.Candidatos.Deputado_Estadual
{
    public class DeputadoEstadualDatabase
    {
        public int Salvar(DeputadoEstadualDTO dto)
        {
            string script = @"INSERT INTO DeputadoEstadual (partido_id_partido,nm_deputadoest,ft_deputadoest,nr_deputadoest) VALUES (@partido_id_partido,@nm_deputadoest,@ft_deputadoest,@nr_deputadoest)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("partido_id_partido", dto.IdPartido));
            parms.Add(new MySqlParameter("nm_deputadoest", dto.Nome));
            parms.Add(new MySqlParameter("ft_deputadoest", dto.Foto));
            parms.Add(new MySqlParameter("nr_deputadoest", dto.Numero));
            

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public DeputadoEstadualDTO Consultar(string numero)
        {
            string script =
            @"SELECT * 
                FROM DeputadoEstadual WHERE nr_deputadoEst like @nr_deputadoEst";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nr_deputadoEst", numero));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            DeputadoEstadualDTO deputado = null;

            if (reader.Read())
            {
                deputado = new DeputadoEstadualDTO();
                deputado.Id = reader.GetInt32("id_dptEstadual");
                deputado.IdPartido = reader.GetInt32("Partido_id_Partido");
                deputado.Nome = reader.GetString("nm_deputadoEst");
                deputado.Numero = reader.GetInt32("nr_deputadoEst");
                deputado.Foto = reader.GetString("ft_deputadoEst");

            }
            reader.Close();

            return deputado;
        }
    }
}
