﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Candidatos
{
    public class PresidenteBusiness
    {
       

        public int Salvar(PresidenteDTO numero)
        {
            PresidenteDatabase db = new PresidenteDatabase();
            if (numero.Numero == 0)
            {
                throw new ArgumentException("Número é obrigatório.");
            }

            return db.Salvar(numero);
        }

        public PresidenteDTO Consultar(string numero)
        {
            PresidenteDatabase db = new PresidenteDatabase();
            return db.Consultar(numero);
        }
    }
}
