﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Base;

namespace Urna.DB.Candidatos
{
    public class PresidenteDatabase
    {
        public int Salvar(PresidenteDTO dto)
        {
            string script = @"INSERT INTO Presidente (nm_presidente,ft_presidente,ft_vice_presidente,nm_vice-presidente,fk_partido_presidente,nr_presidente) VALUES (@nm_presidente,@ft_presidente,@ft_vice_presidente,@nm_vice-presidente,@fk_partido_presidente,@nr_presidente)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_partido_presidente", dto.IdPartido));
            parms.Add(new MySqlParameter("nm_presidente", dto.Nome));
            parms.Add(new MySqlParameter("ft_presidente", dto.Foto));
            parms.Add(new MySqlParameter("ft_vice_presidente", dto.FotoVice));
            parms.Add(new MySqlParameter("nm_vice-presidente", dto.Vice));
            parms.Add(new MySqlParameter("nr_presidente", dto.Numero));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public PresidenteDTO Consultar( string numero)
        {
            string script =
            @"SELECT * 
                FROM Presidente WHERE
                 nr_presidente like @nr_presidente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nr_presidente", numero));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            PresidenteDTO presidente = null;

            if (reader.Read())
            {
                presidente = new PresidenteDTO();
                presidente.Id = reader.GetInt32("Id_presidente");
                presidente.IdPartido = reader.GetInt32("fk_partido_presidente");
                presidente.Nome = reader.GetString("nm_presidente");
                presidente.Numero = reader.GetInt32("nr_presidente");
                presidente.Foto = reader.GetString("ft_presidente");
                presidente.FotoVice = reader.GetString("ft_vice_presidente");
                presidente.Vice = reader.GetString("nm_vice_partido");
            }
            reader.Close();

            return presidente;
        }
    }
}
