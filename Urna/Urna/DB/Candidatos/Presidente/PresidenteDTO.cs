﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Candidatos
{
    public class PresidenteDTO
    {
        public int Id { get; set; }
        public int IdPartido { get; set; }
        public string Nome { get; set; }
        public string Foto { get; set; }
        public string FotoVice { get; set; }
        public string Vice { get; set; }
        public int Numero { get; set; }
    }
}
