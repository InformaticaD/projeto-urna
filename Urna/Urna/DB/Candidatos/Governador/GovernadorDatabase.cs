﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Urna.DB.Base;

namespace Urna.DB.Candidatos.Governador
{
    public class GovernadorDatabase
    {
        public int Salvar(GovernadorDTO dto)
        {
            string script = @"INSERT INTO Governador (nm_governador,ft_Governador,ft_vice_governador,nm_vice-governador,fk_partido_governador,nr_Governador) VALUES (@nm_governador,@ft_Governador,@ft_vice_governador,@nm_vice-governador,@fk_partido_governador,@nr_Governador)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("fk_partido_governador", dto.IdPartido));
            parms.Add(new MySqlParameter("nm_Governador", dto.Nome));
            parms.Add(new MySqlParameter("ft_Governador", dto.Foto));
            parms.Add(new MySqlParameter("ft_vice_governador", dto.FotoVice));
            parms.Add(new MySqlParameter("nm_vice-governador", dto.Vice));
            parms.Add(new MySqlParameter("nr_Governador", dto.Numero));
            

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public GovernadorDTO Consultar(string numero)
        {
            string script =
            @"SELECT * FROM Governador WHERE nr_Governador like @nr_Governador";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nr_Governador", numero));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            GovernadorDTO Governadores = null;

            if (reader.Read())
            {
                Governadores = new GovernadorDTO();
                Governadores.Id = reader.GetInt32("id_governador");
                Governadores.IdPartido = reader.GetInt32("fk_partido_governador");
                Governadores.Nome = reader.GetString("nm_Governador");
                Governadores.Numero = reader.GetInt32("nr_Governador");
                Governadores.Foto = reader.GetString("ft_governador");
                Governadores.FotoVice = reader.GetString("ft_vice_governador");
                Governadores.Vice = reader.GetString("nm_vice_governador");
            }
            reader.Close();

            return Governadores;
        }
    }
}
