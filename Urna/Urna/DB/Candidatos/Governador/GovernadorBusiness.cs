﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Urna.DB.Candidatos.Governador
{
    public class GovernadorBusiness
    {
        

        public int Salvar(GovernadorDTO nome)
        {
            GovernadorDatabase db = new GovernadorDatabase();
            if (nome.Numero == 0)
            {
                throw new ArgumentException("Número é obrigatório.");
            }

            return db.Salvar(nome);
        }

        public GovernadorDTO Consultar(string numero)
        {
            GovernadorDatabase db = new GovernadorDatabase();
            return db.Consultar(numero);
        }
    }
}
